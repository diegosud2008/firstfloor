import React, { Component } from 'react';
import Style from '../../styles/button';

export default class Button extends Component{
    render() {
        let { text, link, ativo} = this.props;
        return(
            <Style>
                <div className="button">
                    <button className={ativo}>
                        {text}
                    </button>
                </div>
            </Style>
        )
    }
}