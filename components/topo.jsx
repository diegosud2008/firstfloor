import React, { Component } from 'react';
import { Menu,Layout } from 'antd';
import { MailOutlined, AppstoreOutlined, SettingOutlined } from '@ant-design/icons';


const { Header } = Layout;
const { SubMenu,Item,ItemGroup } = Menu;

export default class Topo extends Component{
    state = {
        ItensMenu: this.props.PropsMenu || []
    };

    componentDidMount() {
        if (!this.props.PropsMenu) {
            this.setState({
                ItensMenu: [
                    {
                        nome: 'Imoveis para alugar',
                        link: '',
                        key: '1',
                        subMenu: {
                            key: '3',
                            titulo: "Para proprietários",
                            itens: [{
                                Option: "1"
                            }]
                        }
                    },
                    {
                        key: '2',
                        nome: 'laranja'
                    }
                ]
            })
        }
    }

    handleClick = e => {
        console.log('click ', e);
        this.setState({ current: e.key });
    };

    render() {
        const { ItensMenu } = this.state;
        return (
            <Layout>
                <Header className="d-flex flex-wrap b-write" style={{ zIndex: 1, width: '100%',border: '1px solid #f0f0f0' }}>
                    <div className="logo">
                        <img src="" alt="" />
                    </div>
                    <Menu onClick={this.handleClick} mode="horizontal">
                        {
                            ItensMenu.map( (itens,index) => {
                                return(
                                    <>
                                        <Item key={itens.key}>
                                            {itens.nome}
                                        </Item>
                                        {
                                        itens.subMenu && 
                                            <SubMenu key={itens.subMenu.key} title={itens.subMenu.titulo}>
                                                {
                                                    itens.subMenu.itens.map( (sub,indexSub) => {
                                                        return(
                                                            <Item key={indexSub}>{sub.Option}</Item>
                                                            
                                                        )
                                                    })
                                                }
                                            </SubMenu>
                                        }
                                    </>
                                )
                            })
                        }
                    </Menu>
                </Header>
            </Layout>
        );
    }
}