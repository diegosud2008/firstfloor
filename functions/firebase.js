import firebase from "firebase/app";
import "firebase/firestore";
export function Firebase(){
    var firebaseConfig = {
        apiKey: "AIzaSyAQQdWYD7Tx6MH6qIybQ69LWe20wR-zAks",
        authDomain: "museu-les.firebaseapp.com",
        projectId: "museu-les",
        storageBucket: "museu-les.appspot.com",
        messagingSenderId: "117909967033",
        appId: "1:117909967033:web:3f39279dbff69ed4c98b68",
        measurementId: "G-NKKZZ2B5XR"
    };
    if (!firebase.apps.length) {
        firebase.initializeApp(firebaseConfig);
     }else {
        firebase.app(); // if already initialized, use that one
     }
    // Initialize Firebase

    return firebase.firestore();
}