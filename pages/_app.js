import React from "react";
import App from "next/app";
import '../global.css';
import 'antd/dist/antd.css';

export default class extends App {
    static async getInitialProps({ Component, ctx, query}) {
        let pageProps = {};
        if (Component.getInitialProps) {
            pageProps = await Component.getInitialProps(ctx);
        }
        return {
            pageProps: {
                ...pageProps,
            }
        };
    }
    render() {
        const { Component, pageProps } = this.props;
        return (
          <React.Fragment>
              <Component {...pageProps} nprogressStart={() => {NProgress.set(0.4);NProgress.start();}} nprogressEnd={() => NProgress.done(true)} />
        </React.Fragment>
        );
    }
}
