import Head from 'next/head'
import { Row, Col } from 'antd';
import ButtonPerson from '../components/global-components/button';
import Topo from '../components/topo';

export default function Home() {
  
  return (
    <div className="container">
        <Head>
          <title>Create Next App</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
      
        <Row>
          <Col xs={{ span: 24 }} lg={{ span: 24}}>
            teste
          </Col>
        </Row>
    </div>
  )
}
