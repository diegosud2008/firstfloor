import Document, { Head, Main, NextScript } from 'next/document';
import { ServerStyleSheet } from 'styled-components';

export default class MyDocument extends Document {
  static getInitialProps = async (ctx) => {

    // Step 1: Create an instance of ServerStyleSheet
    const sheet = new ServerStyleSheet();

    // Step 2: Retrieve styles from components in the page
    const page = ctx.renderPage(App => props =>
      sheet.collectStyles(<App {...props} />)
    );

    // Step 3: Extract the styles as <style> tags
    const styleTags = sheet.getStyleElement();

    // Step 4: Pass styleTags as a prop

    // Step 5: Pass empresa para os props

    return { ...page, styleTags };
  }
  render() {
    return (
      <html lang="pt-br">
        <Head>
          {/* Step 5: Output the styles in the head  */}
          {this.props.styleTags}
          {/* Google fonts */}
          {/*<link*/}
          {/*    async*/}
          {/*    href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;display=swap"*/}
          {/*    rel="canonical"*/}
          {/*/>*/}
          {/* Favicon */}
          {/* <link
            rel="icon"
            href="https://firebasestorage.googleapis.com/v0/b/smartimob-dev-test.appspot.com/o/images%2Ffavicon.ico?alt=media&token=9f6f5ad8-7f64-472d-aa46-c7ee69f5b766"
          /> */}
          {/*<link rel="manifest" href="/static/manifest.json" />*/}
          {/* scroll.js */}
          {/*<script src="https://firebasestorage.googleapis.com/v0/b/smartimob-dev-test.appspot.com/o/images%2Fscroll.js?alt=media&token=52befb50-016c-4c33-9ab2-c0b0fb530a18"></script>*/}

        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
        <style
          dangerouslySetInnerHTML={{
            __html: `
                html,
                body,
                div,
                span,
                applet,
                object,
                iframe,
                h1,
                h2,
                h3,
                h4,
                h5,
                h6,
                p,
                blockquote,
                pre,
                a,
                abbr,
                acronym,
                address,
                big,
                cite,
                code,
                del,
                dfn,
                em,
                img,
                ins,
                kbd,
                q,
                s,
                samp,
                small,
                strike,
                strong,
                sub,
                sup,
                tt,
                var,
                b,
                u,
                i,
                center,
                dl,
                dt,
                dd,
                ol,
                ul,
                li,
                fieldset,
                form,
                label,
                legend,
                table,
                caption,
                tbody,
                tfoot,
                thead,
                tr,
                th,
                td,
                article,
                aside,
                canvas,
                details,
                embed,
                figure,
                figcaption,
                footer,
                header,
                hgroup,
                menu,
                nav,
                output,
                ruby,
                section,
                summary,
                time,
                mark,
                audio,
                video {
                  margin: 0;
                  padding: 0;
                  border: 0;
                  font-size: 100%;
                  font: inherit;
                  vertical-align: baseline;
                }
                article,
                aside,
                details,
                figcaption,
                figure,
                footer,
                header,
                hgroup,
                menu,
                nav,
                section {
                  display: block;
                }
                body {
                  line-height: 1;
                }
                ol,
                ul {
                  list-style: none;
                }
                blockquote,
                q {
                  quotes: none;
                }
                blockquote:before,
                blockquote:after,
                q:before,
                q:after {
                  content: "";
                  content: none;
                }
                table {
                  border-collapse: collapse;
                  border-spacing: 0;
                }
                @import url('https://fonts.googleapis.com/css2?family=Barlow+Semi+Condensed:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,900&family=Kodchasan&family=Lato&family=Montserrat&family=Open+Sans&family=Oswald&family=Raleway&family=Roboto&family=Roboto+Condensed&family=Source+Sans+Pro&display=swap');
                    *,body,#root,html {
                      margin: 0;
                      padding: 0;
                      box-sizing: border-box;
                      font-family: 'Barlow Semi Condensed', sans-serif;
                    }
              `
          }}
        />

      </html>
    );
  }
}
