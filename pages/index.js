import Head from 'next/head'
import { Row, Col } from 'antd';
import ButtonPerson from '../components/global-components/button';
import Topo from '../components/topo';
import Firebase from 'firebase';

const ItensMenu = [
  {
      nome: 'Imoveis para alugar',
      link: '',
      key: '1',
      subMenu: {
          key: '3',
          titulo: "Para proprietários",
          itens: [{
              Option: "1"
          }]
      }
  },
  {
      key: '2',
      nome: 'laranja'
  }
];
export default function Home() {
  return (
    <div className="container">
        <Head>
          <title>Create Next App</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
      
        <Row>
          <Col xs={{ span: 24 }} lg={{ span: 24}}>
            <Topo PropsMenu={ItensMenu}/>
          </Col>
        </Row>
    </div>
  )
}
