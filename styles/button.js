import styled from 'styled-components';

const Wrapper = styled.div`
button {
    width: 100%;
    display: inherit;
    align-items: inherit;
    justify-content: inherit;
    padding: 13px;
    border: 0;
}
.ativo {
    cursor: pointer;
    outline: 0;
    -webkit-transition: color .6 ease;
    transition: color .6 ease;
    background: 0 0;
    border: 0;
    background-color: #b3d4fc;
    border-radius: 7px;
    color: #ffff;
    font-weight: 700;
}
`

export default Wrapper