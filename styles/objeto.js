import styled from 'styled-components';

const Wrapper = styled.div`
.infos {
        justify-content: space-between;
    }
.card-person {
    background: #f3f3f3;
    padding: 4px 12px;
    p {
        margin: 0;
    }
    .titulo {
        width: 100%;
        display: block;
        height: 40px;
    }
    .img {
        width: 125px;
        display: block;
    }
    .c-red {
        color: red;
    }
}
`

export default Wrapper