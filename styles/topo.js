import styled from 'styled-components';

const Wrapper = styled.div`
.top {
    z-index: 1;
    background: linear-gradient(
180deg
, rgba(2,0,36,1) 0%, rgb(38 38 78) 10%, rgba(0,212,255,0) 100%);
    width: 100%;
    position: absolute;

    ul {
        display: flex;
        padding: 14px 105px;
        list-style: none;
        justify-content: flex-end;
        color: #fff;

        li {
            margin-right: 50px;
        }
    }
}
`

export default Wrapper