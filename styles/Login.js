import styled from 'styled-components';

const Wrapper = styled.div`
color: #0e2331;
.gutter-row {
    background: linear-gradient(to top right, #03031d , #24507d);
}
#left {
    img {
        /* width: 100%; */
        margin-top: 18vh;
        margin-left: 25vw;
    }
}
.top {
    padding: 50px 0;
    /* background: #0e2331; */
    display: flex;
    justify-content: center;
}
.card {    
    height: 100vh;
    background: #fff;
    width: 70%;
    padding: 65px 0;
}
@media screen and (max-width: 560px) {
    .card {
        width: 100%;
    }
}
.body {
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    text-align: center;
    padding: 30px 0;
    height: 68.5vh;

    h1,h2,h3,h4,h5 {
        width: 100%;
        padding: 0;
        margin: 0;
    }
    #login {
        width: 80%;
        padding-top: 40px;

        button {
            width: 100%;
            height: 70px;
            font-size: 25px;
        }
        input {
            height: 48px;
        }
    }
    #end-body {
        display: flex;
        flex-wrap: wrap;
        align-items: center;
        hr {
            width: 100%;
        }
        p {
            width: 100%;
            text-align: center;
            font-weight: bold;
        }
        .links {
            display: flex;
            justify-content: space-around;
            width: 100%;
            flex-wrap: wrap;
            a {
                color: #0e2331;
                width: 100%;
            }
        }
    }
}
footer {
    /* background: #0e2331; */
    /* color: #fff; */
    display: flex;
    flex-wrap: wrap;
    align-items: center;
    justify-content: space-between;
    padding: 10px 40px;
}
`

export default Wrapper